package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IEntityManagerService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
public final class UserService implements IUserService {

    @NotNull
    final IEntityManagerService entityManagerService;

    @Nullable
    @Override
    public UserEntity create(@Nullable final UserEntity userEntity) throws Exception {
        if (!isEntityValid(userEntity)) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try{
            userRepository.persist(userEntity);
            entityManager.getTransaction().commit();
            return userEntity;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    public boolean checkRole(@Nullable final String userId, @Nullable final RoleType[] roles) {
        if (roles == null) return false;
        if (userId == null || userId.isEmpty()) return false;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserEntity userEntity = userRepository.findOneById(userId);
        entityManager.close();
        if (userEntity == null) return false;
        return Arrays.asList(roles).contains(userEntity.getRoleType());
    }

    @Nullable
    @Override
    public String getUserId(int userIndex) throws Exception {
        if (userIndex < 0) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @NotNull final List<UserEntity> userEntities = userRepository.findAll();
        entityManager.close();
        return userIndex >= userEntities.size() ? null : userEntities.get(userIndex).getId();
    }

    public boolean isEntityValid(@Nullable final UserEntity userEntity) {
        if (userEntity == null || userEntity.getRoleType() == null) return false;
        if (userEntity.getId().isEmpty()) return false;
        if (userEntity.getLogin() == null || userEntity.getLogin().isEmpty()) return false;
        return userEntity.getPwdHash() != null && !userEntity.getPwdHash().isEmpty();
    }

    @Nullable
    @Override
    public UserEntity findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserEntity userEntity = userRepository.findOneById(id);
        entityManager.close();
        return userEntity;
    }

    @Nullable
    @Override
    public UserEntity edit(@Nullable final String userId, @Nullable final UserEntity userEntity) throws Exception {
        if (!isEntityValid(userEntity)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(userEntity.getId())) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.merge(userEntity);
        entityManager.getTransaction().commit();
        entityManager.close();
        return userEntity;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try{
            userRepository.removeAllByUserId(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try{
            userRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<UserEntity> findAll() throws Exception {
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @NotNull final List<UserEntity> userEntities = userRepository.findAll();
        entityManager.close();
        return userEntities;
    }

}
