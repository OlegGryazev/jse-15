package ru.gryazev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.Status;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
public final class Project extends AbstractCrudDTO implements ComparableEntity {

    @Nullable
    private String userId = null;

    @Nullable
    private String name = "";

    @Nullable
    private String details = "";

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Long createMillis = new Date().getTime();

    @Nullable
    public static ProjectEntity toProjectEntity(
            @NotNull final ServiceLocator serviceLocator,
            @Nullable final Project project
    ) {
        if (project == null) return null;
        @Nullable final UserEntity userEntity = serviceLocator.getUserService().findOne(project.getUserId());
        if (userEntity == null) return null;
        @NotNull final ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(project.getId());
        projectEntity.setName(project.getName());
        projectEntity.setDetails(project.getDetails());
        projectEntity.setDateStart(project.getDateStart());
        projectEntity.setDateFinish(project.getDateFinish());
        projectEntity.setStatus(project.getStatus());
        projectEntity.setTasks(serviceLocator.getTaskService().findByProjectId(project.getUserId(), project.getId()));
        projectEntity.setUser(userEntity);
        projectEntity.setCreateMillis(project.getCreateMillis());
        return projectEntity;
    }

}
