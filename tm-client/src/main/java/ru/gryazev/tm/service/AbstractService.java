package ru.gryazev.tm.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.api.service.IService;
import ru.gryazev.tm.entity.AbstractCrudEntity;

public abstract class AbstractService <T extends AbstractCrudEntity> implements IService<T> {

    @Nullable
    @Override
    public T edit(@Nullable final String userId, @Nullable final T t) {
        if (!isEntityValid(t)) return null;
        if (userId == null || userId.isEmpty()) return null;
        return getRepository().merge(userId, t);
    }

    @Contract("null -> false")
    public abstract boolean isEntityValid(@Nullable final T t);

    @NotNull
    public abstract IRepository<T> getRepository();

}
