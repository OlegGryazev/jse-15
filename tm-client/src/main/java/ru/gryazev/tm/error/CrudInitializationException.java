package ru.gryazev.tm.error;

public class CrudInitializationException extends CrudException {

    public CrudInitializationException() {
        super("Error during command initialization.");
    }

}
