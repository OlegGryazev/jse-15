package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudInitializationException;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final String token = getToken();
        @NotNull final User currentUser = userEndpoint.findCurrentUser(token);
        if (currentUser == null) throw new CrudInitializationException();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectEndpoint.getProjectId(token, projectIndex);
        if (projectId == null) throw new CrudNotFoundException();

        @NotNull final Project project = terminalService.getProjectFromConsole();
        project.setUserId(currentUser.getId());
        project.setId(projectId);
        @Nullable final Project editedProject = projectEndpoint.editProject(token, project);
        if (editedProject == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
