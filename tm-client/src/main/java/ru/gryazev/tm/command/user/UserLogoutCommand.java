package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null || serviceLocator == null || sessionLocator == null) return;
        @Nullable final String token = getToken();
        serviceLocator.getSessionEndpoint().removeSession(token);
        sessionLocator.setToken(null);
        terminalService.print("You are logged out.");
    }

}
