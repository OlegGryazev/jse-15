package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@NoArgsConstructor
public final class TaskListUnlinkedCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-list-unlinked";
    }

    @Override
    public String getDescription() {
        return "Shows all unlinked tasks.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null || settingService == null) return;
        @NotNull final String token = getToken();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final String sortType = settingService.findValueByKey("project-sort");
        @NotNull final List<Task> tasks = taskEndpoint.listTaskUnlinkedSorted(token, sortType);
        if (tasks.isEmpty()) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++)
            terminalService.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
