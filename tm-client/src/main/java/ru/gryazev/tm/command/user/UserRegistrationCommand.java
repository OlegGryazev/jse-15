package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.error.CrudUpdateException;

public final class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "Registration of new user.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final User user = terminalService.getUserPwdRepeat();
        if (user == null) throw new CrudUpdateException();
        @Nullable final User createdUser = serviceLocator.getUserEndpoint().createUser(user);
        if (createdUser == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}
